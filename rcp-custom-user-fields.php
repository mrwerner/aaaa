<?php
/*
Plugin Name: Restrict Content Pro - Custom User Fields
Description: Illustrates how to add custom user fields to the Restrict Content Pro registration form that can also be edited by the site admins
Version: 1.0
Author: Pippin Williamson
Author URI: http://pippinsplugins.com
Contributors: mordauk
*/



/**
 * 1 Adds the custom fields to the registration form and profile editor
 *
 */
function pw_rcp_add_user_fields() {

        /**$jobtitle = get_user_meta( get_current_user_id(), 'rcp_jobtitle', true );*/
        $companyname   = get_user_meta( get_current_user_id(), 'rcp_companyname', true );
        $telephone   = get_user_meta( get_current_user_id(), 'rcp_telephone', true );
        $address1   = get_user_meta( get_current_user_id(), 'rcp_address1', true );
        $address2   = get_user_meta( get_current_user_id(), 'rcp_address2', true );
        $postcode   = get_user_meta( get_current_user_id(), 'rcp_postcode', true );
        $city   = get_user_meta( get_current_user_id(), 'rcp_city', true );
        $state   = get_user_meta( get_current_user_id(), 'rcp_state', true );
        $country   = get_user_meta( get_current_user_id(), 'rcp_country', true );
        $dob   = get_user_meta( get_current_user_id(), 'rcp_dob', true );
        $website   = get_user_meta( get_current_user_id(), 'rcp_website', true );
        $gender   = get_user_meta( get_current_user_id(), 'rcp_gender', true );
        ?>

        <?php if (is_page('supporter-membership-registration')): ?>
          <p id="rcp_companyname_wrap">
                  <label for="rcp_companyname"><?php _e( 'If Company/Organisation name', 'rcp' ); ?></label>
                  <!--<input name="rcp_companyname" id="rcp_companyname" type="text" value="<?php //echo esc_attr( $companyname ); ?>"/>-->

                  <input name="rcp_companyname" id="rcp_companyname" type="text" value="<?php if (!empty($_POST['$companyname'])) { echo htmlentities($_POST['$companyname']); } ?>" />
          </p>
        <?php endif; ?>


         <?php if (is_page('artist-membership-registration')): ?>
          <p id="rcp_dob_wrap">
                  <label for="rcp_dob"><?php _e( 'Date of Birth', 'rcp' ); ?></label>
                  <!--<input name="rcp_companyname" id="rcp_companyname" type="text" value="<?php //echo esc_attr( $companyname ); ?>"/>-->

                  <input name="rcp_dob" id="rcp_date" type="text" value="<?php if (!empty($_POST['$dob'])) { echo htmlentities($_POST['$dob']); } ?>" />
                  <p class="description"><?php _e( 'format:mm-dd-yyyy', 'rcp' ); ?></p>
          </p>

          <p id="rcp_gender_wrap">
                  <label for="rcp_gender"><?php _e( 'Gender', 'rcp' ); ?></label>

                  <input name="rcp_gender" id="rcp_gender" type="text" value="<?php if (!empty($_POST['$gender'])) { echo htmlentities($_POST['$gender']); } ?>" />
          </p>
        <?php endif; ?>

        <p id="rcp_website_wrap">
                <label for="rcp_website"><?php _e( 'Website', 'rcp' ); ?></label>
                <input name="rcp_website" id="rcp_website" type="text"  value="<?php echo esc_attr( $website ); ?>"/>
        </p>
        <p id="rcp_telephone_wrap">
                <label for="rcp_telephone"><?php _e( 'Telephone add your area code', 'rcp' ); ?></label>
                <input name="rcp_telephone" id="rcp_telephone" type="text"  value="<?php echo esc_attr( $telephone ); ?>"/>
        </p>
        <p id="rcp_address1_wrap">
                <label for="rcp_address1"><?php _e( 'Address 1', 'rcp' ); ?></label>
                <input name="rcp_address1" id="rcp_address1" type="text" value="<?php echo esc_attr( $address1 ); ?>"/>
        </p>
        <p id="rcp_address2_wrap">
                <label for="rcp_address2"><?php _e( 'Address 2', 'rcp' ); ?></label>
                <input name="rcp_address2" id="rcp_address2" type="text" value="<?php echo esc_attr( $address2 ); ?>"/>
        </p>
        <p id="rcp_city_wrap">
                <label for="rcp_city"><?php _e( 'City', 'rcp' ); ?></label>
                <input name="rcp_city" id="rcp_city" type="text" value="<?php echo esc_attr( $city ); ?>"/>
        </p>
        <p id="rcp_postcode_wrap">
                <label for="rcp_postcode"><?php _e( 'Post Code', 'rcp' ); ?></label>
                <input name="rcp_postcode" id="rcp_postcode" type="text" value="<?php echo esc_attr( $postcode ); ?>"/>
        </p>
        <p id="rcp_state_wrap">
                <label for="rcp_state"><?php _e( 'State', 'rcp' ); ?></label>
                <input name="rcp_state" id="rcp_state" type="text" value="<?php echo esc_attr( $state ); ?>"/>
        </p>
            <!--country start-->
        <div class="country-styled-select">
          <p id="rcp_country_wrap">
                  <label for="rcp_country">Select Country</label>
                  <?php
                  //get dropdown saved value
                 
                  $selected = get_the_author_meta( 'rcp_country', $user->ID );
                  ?>
                      <select name="rcp_country" id="rcp_country" class="required">
                                  <option <?php selected( $country, 'Australia' ) ?> value="Australia" data-alternative-spellings="AU" data-relevancy-booster="1.5">Australia</option>
                                  <option <?php selected( $country, 'Åland Islands' ) ?> value="Åland Islands" data-alternative-spellings="AX Aaland Aland" data-relevancy-booster="0.5">Åland Islands</option>
                                  <option <?php selected( $country, 'Albania' ) ?> value="Albania" data-alternative-spellings="AL">Albania</option>
                                  <option <?php selected( $country, 'Algeria' ) ?> value="Algeria" data-alternative-spellings="DZ الجزائر">Algeria</option>
                                  <option <?php selected( $country, 'American Samoa' ) ?> value="American Samoa" data-alternative-spellings="AS" data-relevancy-booster="0.5">American Samoa</option>
                                  <option <?php selected( $country, 'Andorra' ) ?> value="Andorra" data-alternative-spellings="AD" data-relevancy-booster="0.5">Andorra</option>
                                  <option <?php selected( $country, 'Angola' ) ?> value="Angola" data-alternative-spellings="AO">Angola</option>
                                  <option <?php selected( $country, 'Anguilla' ) ?> value="Anguilla" data-alternative-spellings="AI" data-relevancy-booster="0.5">Anguilla</option>
                                  <option <?php selected( $country, 'Antarctica' ) ?> value="Antarctica" data-alternative-spellings="AQ" data-relevancy-booster="0.5">Antarctica</option>
                                  <option <?php selected( $country, 'Antigua And Barbuda' ) ?> value="Antigua And Barbuda" data-alternative-spellings="AG" data-relevancy-booster="0.5">Antigua And Barbuda</option>
                                  <option <?php selected( $country, 'Argentina' ) ?> value="Argentina" data-alternative-spellings="AR">Argentina</option>
                                  <option <?php selected( $country, 'Armenia' ) ?> value="Armenia" data-alternative-spellings="AM Հայաստան">Armenia</option>
                                  <option <?php selected( $country, 'Aruba' ) ?> value="Aruba" data-alternative-spellings="AW" data-relevancy-booster="0.5">Aruba</option>
                                  <option <?php selected( $country, 'Australia' ) ?> value="Australia" data-alternative-spellings="AU" data-relevancy-booster="1.5">Australia</option>
                                  <option <?php selected( $country, 'Austria' ) ?> value="Austria" data-alternative-spellings="AT Österreich Osterreich Oesterreich ">Austria</option>
                                  <option <?php selected( $country, 'Azerbaijan' ) ?> value="Azerbaijan" data-alternative-spellings="AZ">Azerbaijan</option>
                                  <option <?php selected( $country, 'Bahamas' ) ?> value="Bahamas" data-alternative-spellings="BS">Bahamas</option>
                                  <option <?php selected( $country, 'Bahrain' ) ?> value="Bahrain" data-alternative-spellings="BH البحرين">Bahrain</option>
                                  <option <?php selected( $country, 'Bangladesh' ) ?> value="Bangladesh" data-alternative-spellings="BD বাংলাদেশ" data-relevancy-booster="2">Bangladesh</option>
                                  <option <?php selected( $country, 'Barbados' ) ?> value="Barbados" data-alternative-spellings="BB">Barbados</option>
                                  <option <?php selected( $country, 'Belarus' ) ?> value="Belarus" data-alternative-spellings="BY Беларусь">Belarus</option>
                                  <option <?php selected( $country, 'Belgium' ) ?> value="Belgium" data-alternative-spellings="BE België Belgie Belgien Belgique" data-relevancy-booster="1.5">Belgium</option>
                                  <option <?php selected( $country, 'Belize' ) ?> value="Belize" data-alternative-spellings="BZ">Belize</option>
                                  <option <?php selected( $country, 'Benin' ) ?> value="Benin" data-alternative-spellings="BJ">Benin</option>
                                  <option <?php selected( $country, 'Bermuda' ) ?> value="Bermuda" data-alternative-spellings="BM" data-relevancy-booster="0.5">Bermuda</option>
                                  <option <?php selected( $country, 'Bhutan' ) ?> value="Bhutan" data-alternative-spellings="BT भूटान">Bhutan</option>
                                  <option <?php selected( $country, 'Bolivia' ) ?> value="Bolivia" data-alternative-spellings="BO">Bolivia</option>
                                  <option <?php selected( $country, 'Bonaire, Sint Eustatius and Saba' ) ?> value="Bonaire, Sint Eustatius and Saba" data-alternative-spellings="BQ">Bonaire, Sint Eustatius and Saba</option>
                                  <option <?php selected( $country, 'Bosnia and Herzegovina' ) ?> value="Bosnia and Herzegovina" data-alternative-spellings="BA Босна и Херцеговина">Bosnia and Herzegovina</option>
                                  <option <?php selected( $country, 'Botswana' ) ?> value="Botswana" data-alternative-spellings="BW">Botswana</option>
                                  <option <?php selected( $country, 'Bouvet Island' ) ?> value="Bouvet Island" data-alternative-spellings="BV">Bouvet Island</option>
                                  <option <?php selected( $country, 'Brazil' ) ?> value="Brazil" data-alternative-spellings="BR Brasil" data-relevancy-booster="2">Brazil</option>
                                  <option <?php selected( $country, 'British Indian Ocean Territory' ) ?> value="British Indian Ocean Territory" data-alternative-spellings="IO">British Indian Ocean Territory</option>
                                  <option <?php selected( $country, 'Brunei Darussalam' ) ?> value="Brunei Darussalam" data-alternative-spellings="BN">Brunei Darussalam</option>
                                  <option <?php selected( $country, 'Bulgaria' ) ?> value="Bulgaria" data-alternative-spellings="BG България">Bulgaria</option>
                                  <option <?php selected( $country, 'Burkina Faso' ) ?> value="Burkina Faso" data-alternative-spellings="BF">Burkina Faso</option>
                                  <option <?php selected( $country, 'Burundi' ) ?> value="Burundi" data-alternative-spellings="BI">Burundi</option>
                                  <option <?php selected( $country, 'Cambodia' ) ?> value="Cambodia" data-alternative-spellings="KH កម្ពុជា">Cambodia</option>
                                  <option <?php selected( $country, 'Cameroon' ) ?> value="Cameroon" data-alternative-spellings="CM">Cameroon</option>
                                  <option <?php selected( $country, 'Canada' ) ?> value="Canada" data-alternative-spellings="CA" data-relevancy-booster="2">Canada</option>
                                  <option <?php selected( $country, 'Cape Verde' ) ?> value="Cape Verde" data-alternative-spellings="CV Cabo">Cape Verde</option>
                                  <option <?php selected( $country, 'Cayman Islands' ) ?> value="Cayman Islands" data-alternative-spellings="KY" data-relevancy-booster="0.5">Cayman Islands</option>
                                  <option <?php selected( $country, 'Central African Republic' ) ?> value="Central African Republic" data-alternative-spellings="CF">Central African Republic</option>
                                  <option <?php selected( $country, 'Chad' ) ?> value="Chad" data-alternative-spellings="TD تشاد‎ Tchad">Chad</option>
                                  <option <?php selected( $country, 'Chile' ) ?> value="Chile" data-alternative-spellings="CL">Chile</option>
                                  <option <?php selected( $country, 'China' ) ?> value="China" data-relevancy-booster="3.5" data-alternative-spellings="CN Zhongguo Zhonghua Peoples Republic 中国/中华">China</option>
                                  <option <?php selected( $country, 'Christmas Island' ) ?> value="Christmas Island" data-alternative-spellings="CX" data-relevancy-booster="0.5">Christmas Island</option>
                                  <option <?php selected( $country, 'Cocos (Keeling) Islands' ) ?> value="Cocos (Keeling) Islands" data-alternative-spellings="CC" data-relevancy-booster="0.5">Cocos (Keeling) Islands</option>
                                  <option <?php selected( $country, 'Colombia' ) ?> value="Colombia" data-alternative-spellings="CO">Colombia</option>
                                  <option <?php selected( $country, 'Comoros' ) ?> value="Comoros" data-alternative-spellings="KM جزر القمر">Comoros</option>
                                  <option <?php selected( $country, 'Congo' ) ?> value="Congo" data-alternative-spellings="CG">Congo</option>
                                  <option <?php selected( $country, 'Congo, the Democratic Republic of the' ) ?> value="Congo, the Democratic Republic of the" data-alternative-spellings="CD Congo-Brazzaville Repubilika ya Kongo">Congo, the Democratic Republic of the</option>
                                  <option <?php selected( $country, 'Cook Islands' ) ?> value="Cook Islands" data-alternative-spellings="CK" data-relevancy-booster="0.5">Cook Islands</option>
                                  <option <?php selected( $country, 'Costa Rica' ) ?> value="Costa Rica" data-alternative-spellings="CR">Costa Rica</option>
                                  <option <?php selected( $country, "Côte d'Ivoire" ) ?> value="Côte d'Ivoire" data-alternative-spellings="CI Cote dIvoire">Côte d'Ivoire</option>
                                  <option <?php selected( $country, 'Croatia' ) ?> value="Croatia" data-alternative-spellings="HR Hrvatska">Croatia</option>
                                  <option <?php selected( $country, 'Cuba' ) ?> value="Cuba" data-alternative-spellings="CU">Cuba</option>
                                  <option <?php selected( $country, 'Curaçao' ) ?> value="Curaçao" data-alternative-spellings="CW Curacao">Curaçao</option>
                                  <option <?php selected( $country, 'Cyprus' ) ?> value="Cyprus" data-alternative-spellings="CY Κύπρος Kýpros Kıbrıs">Cyprus</option>
                                  <option <?php selected( $country, 'Czech Republic' ) ?> value="Czech Republic" data-alternative-spellings="CZ Česká Ceska">Czech Republic</option>
                                  <option <?php selected( $country, 'Denmark' ) ?> value="Denmark" data-alternative-spellings="DK Danmark" data-relevancy-booster="1.5">Denmark</option>
                                  <option <?php selected( $country, 'Djibouti' ) ?> value="Djibouti" data-alternative-spellings="DJ جيبوتي‎ Jabuuti Gabuuti">Djibouti</option>
                                  <option <?php selected( $country, 'Dominica' ) ?> value="Dominica" data-alternative-spellings="DM Dominique" data-relevancy-booster="0.5">Dominica</option>
                                  <option <?php selected( $country, 'Dominican Republic' ) ?> value="Dominican Republic" data-alternative-spellings="DO">Dominican Republic</option>
                                  <option <?php selected( $country, 'Ecuador' ) ?> value="Ecuador" data-alternative-spellings="EC">Ecuador</option>
                                  <option <?php selected( $country, 'Egypt' ) ?> value="Egypt" data-alternative-spellings="EG" data-relevancy-booster="1.5">Egypt</option>
                                  <option <?php selected( $country, 'El Salvador' ) ?> value="El Salvador" data-alternative-spellings="SV">El Salvador</option>
                                  <option <?php selected( $country, 'Equatorial Guinea' ) ?> value="Equatorial Guinea" data-alternative-spellings="GQ">Equatorial Guinea</option>
                                  <option <?php selected( $country, 'Eritrea' ) ?> value="Eritrea" data-alternative-spellings="ER إرتريا ኤርትራ">Eritrea</option>
                                  <option <?php selected( $country, 'Estonia' ) ?> value="Estonia" data-alternative-spellings="EE Eesti">Estonia</option>
                                  <option <?php selected( $country, 'Ethiopia' ) ?> value="Ethiopia" data-alternative-spellings="ET ኢትዮጵያ">Ethiopia</option>
                                  <option <?php selected( $country, 'Falkland Islands (Malvinas)' ) ?> value="Falkland Islands (Malvinas)" data-alternative-spellings="FK" data-relevancy-booster="0.5">Falkland Islands (Malvinas)</option>
                                  <option <?php selected( $country, 'Faroe Islands' ) ?> value="Faroe Islands" data-alternative-spellings="FO Føroyar Færøerne" data-relevancy-booster="0.5">Faroe Islands</option>
                                  <option <?php selected( $country, 'Fiji' ) ?> value="Fiji" data-alternative-spellings="FJ Viti फ़िजी">Fiji</option>
                                  <option <?php selected( $country, 'Finland' ) ?> value="Finland" data-alternative-spellings="FI Suomi">Finland</option>
                                  <option <?php selected( $country, 'France' ) ?> value="France" data-alternative-spellings="FR République française" data-relevancy-booster="2.5">France</option>
                                  <option <?php selected( $country, 'French Guiana' ) ?> value="French Guiana" data-alternative-spellings="GF">French Guiana</option>
                                  <option <?php selected( $country, 'French Polynesia' ) ?> value="French Polynesia" data-alternative-spellings="PF Polynésie française">French Polynesia</option>
                                  <option <?php selected( $country, 'French Southern Territories' ) ?> value="French Southern Territories" data-alternative-spellings="TF">French Southern Territories</option>
                                  <option <?php selected( $country, 'Gabon' ) ?> value="Gabon" data-alternative-spellings="GA République Gabonaise">Gabon</option>
                                  <option <?php selected( $country, 'Gambia' ) ?> value="Gambia" data-alternative-spellings="GM">Gambia</option>
                                  <option <?php selected( $country, 'Georgia' ) ?> value="Georgia" data-alternative-spellings="GE საქართველო">Georgia</option>
                                  <option <?php selected( $country, 'Germany' ) ?> value="Germany" data-alternative-spellings="DE Bundesrepublik Deutschland" data-relevancy-booster="3">Germany</option>
                                  <option <?php selected( $country, 'Ghana' ) ?> value="Ghana" data-alternative-spellings="GH">Ghana</option>
                                  <option <?php selected( $country, 'Gibraltar' ) ?> value="Gibraltar" data-alternative-spellings="GI" data-relevancy-booster="0.5">Gibraltar</option>
                                  <option <?php selected( $country, 'Greece' ) ?> value="Greece" data-alternative-spellings="GR Ελλάδα" data-relevancy-booster="1.5">Greece</option>
                                  <option <?php selected( $country, 'Greenland' ) ?> value="Greenland" data-alternative-spellings="GL grønland" data-relevancy-booster="0.5">Greenland</option>
                                  <option <?php selected( $country, 'Grenada' ) ?> value="Grenada" data-alternative-spellings="GD">Grenada</option>
                                  <option <?php selected( $country, 'Guadeloupe' ) ?> value="Guadeloupe" data-alternative-spellings="GP">Guadeloupe</option>
                                  <option <?php selected( $country, 'Guam' ) ?> value="Guam" data-alternative-spellings="GU">Guam</option>
                                  <option <?php selected( $country, 'Guatemala' ) ?> value="Guatemala" data-alternative-spellings="GT">Guatemala</option>
                                  <option <?php selected( $country, 'Guernsey' ) ?> value="Guernsey" data-alternative-spellings="GG" data-relevancy-booster="0.5">Guernsey</option>
                                  <option <?php selected( $country, 'Guinea' ) ?> value="Guinea" data-alternative-spellings="GN">Guinea</option>
                                  <option <?php selected( $country, 'Guinea-Bissau' ) ?> value="Guinea-Bissau" data-alternative-spellings="GW">Guinea-Bissau</option>
                                  <option <?php selected( $country, 'Guyana' ) ?> value="Guyana" data-alternative-spellings="GY">Guyana</option>
                                  <option <?php selected( $country, 'Haiti' ) ?> value="Haiti" data-alternative-spellings="HT">Haiti</option>
                                  <option <?php selected( $country, 'Heard Island and McDonald Islands' ) ?> value="Heard Island and McDonald Islands" data-alternative-spellings="HM">Heard Island and McDonald Islands</option>
                                  <option <?php selected( $country, 'Holy See (Vatican City State)' ) ?> value="Holy See (Vatican City State)" data-alternative-spellings="VA" data-relevancy-booster="0.5">Holy See (Vatican City State)</option>
                                  <option <?php selected( $country, 'Honduras' ) ?> value="Honduras" data-alternative-spellings="HN">Honduras</option>
                                  <option <?php selected( $country, 'Hong Kong' ) ?> value="Hong Kong" data-alternative-spellings="HK 香港">Hong Kong</option>
                                  <option <?php selected( $country, 'Hungary' ) ?> value="Hungary" data-alternative-spellings="HU Magyarország">Hungary</option>
                                  <option <?php selected( $country, 'Iceland' ) ?> value="Iceland" data-alternative-spellings="IS Island">Iceland</option>
                                  <option <?php selected( $country, 'India' ) ?> value="India" data-alternative-spellings="IN भारत गणराज्य Hindustan" data-relevancy-booster="3">India</option>
                                  <option <?php selected( $country, 'Indonesia' ) ?> value="Indonesia" data-alternative-spellings="ID" data-relevancy-booster="2">Indonesia</option>
                                  <option <?php selected( $country, 'Iran, Islamic Republic of' ) ?> value="Iran, Islamic Republic of" data-alternative-spellings="IR ایران">Iran, Islamic Republic of</option>
                                  <option <?php selected( $country, 'Iraq' ) ?> value="Iraq" data-alternative-spellings="IQ العراق‎">Iraq</option>
                                  <option <?php selected( $country, 'Ireland' ) ?> value="Ireland" data-alternative-spellings="IE Éire" data-relevancy-booster="1.2">Ireland</option>
                                  <option <?php selected( $country, 'Isle of Man' ) ?> value="Isle of Man" data-alternative-spellings="IM" data-relevancy-booster="0.5">Isle of Man</option>
                                  <option <?php selected( $country, 'Israel' ) ?> value="Israel" data-alternative-spellings="IL إسرائيل ישראל">Israel</option>
                                  <option <?php selected( $country, 'Italy' ) ?> value="Italy" data-alternative-spellings="IT Italia" data-relevancy-booster="2">Italy</option>
                                  <option <?php selected( $country, 'Jamaica' ) ?> value="Jamaica" data-alternative-spellings="JM">Jamaica</option>
                                  <option <?php selected( $country, 'Japan' ) ?> value="Japan" data-alternative-spellings="JP Nippon Nihon 日本" data-relevancy-booster="2.5">Japan</option>
                                  <option <?php selected( $country, 'Jersey' ) ?> value="Jersey" data-alternative-spellings="JE" data-relevancy-booster="0.5">Jersey</option>
                                  <option <?php selected( $country, 'Jordan' ) ?> value="Jordan" data-alternative-spellings="JO الأردن">Jordan</option>
                                  <option <?php selected( $country, 'Kazakhstan' ) ?> value="Kazakhstan" data-alternative-spellings="KZ Қазақстан Казахстан">Kazakhstan</option>
                                  <option <?php selected( $country, 'Kenya' ) ?> value="Kenya" data-alternative-spellings="KE">Kenya</option>
                                  <option <?php selected( $country, 'Kiribati' ) ?> value="Kiribati" data-alternative-spellings="KI">Kiribati</option>
                                  <option <?php selected( $country, "Korea, Democratic People's Republic of" ) ?> value="Korea, Democratic People's Republic of" data-alternative-spellings="KP North Korea">Korea, Democratic People's Republic of</option>
                                  <option <?php selected( $country, 'Korea, Republic of' ) ?> value="Korea, Republic of" data-alternative-spellings="KR South Korea" data-relevancy-booster="1.5">Korea, Republic of</option>
                                  <option <?php selected( $country, 'Kuwait' ) ?> value="Kuwait" data-alternative-spellings="KW الكويت">Kuwait</option>
                                  <option <?php selected( $country, 'Kyrgyzstan' ) ?> value="Kyrgyzstan" data-alternative-spellings="KG Кыргызстан">Kyrgyzstan</option>
                                  <option <?php selected( $country, "Lao People's Democratic Republic" ) ?> value="Lao People's Democratic Republic" data-alternative-spellings="LA">Lao People's Democratic Republic</option>
                                  <option <?php selected( $country, 'Latvia' ) ?> value="Latvia" data-alternative-spellings="LV Latvija">Latvia</option>
                                  <option <?php selected( $country, 'Lebanon' ) ?> value="Lebanon" data-alternative-spellings="LB لبنان">Lebanon</option>
                                  <option <?php selected( $country, 'Lesotho' ) ?> value="Lesotho" data-alternative-spellings="LS">Lesotho</option>
                                  <option <?php selected( $country, 'Liberia' ) ?> value="Liberia" data-alternative-spellings="LR">Liberia</option>
                                  <option <?php selected( $country, 'Libyan Arab Jamahiriya' ) ?> value="Libyan Arab Jamahiriya" data-alternative-spellings="LY ليبيا">Libyan Arab Jamahiriya</option>
                                  <option <?php selected( $country, 'Liechtenstein' ) ?> value="Liechtenstein" data-alternative-spellings="LI">Liechtenstein</option>
                                  <option <?php selected( $country, 'Lithuania' ) ?> value="Lithuania" data-alternative-spellings="LT Lietuva">Lithuania</option>
                                  <option <?php selected( $country, 'Luxembourg' ) ?> value="Luxembourg" data-alternative-spellings="LU">Luxembourg</option>
                                  <option <?php selected( $country, 'Macao' ) ?> value="Macao" data-alternative-spellings="MO">Macao</option>
                                  <option <?php selected( $country, 'Macedonia, The Former Yugoslav Republic Of' ) ?> value="Macedonia, The Former Yugoslav Republic Of" data-alternative-spellings="MK Македонија">Macedonia, The Former Yugoslav Republic Of</option>
                                  <option <?php selected( $country, 'Madagascar' ) ?> value="Madagascar" data-alternative-spellings="MG Madagasikara">Madagascar</option>
                                  <option <?php selected( $country, 'Malawi' ) ?> value="Malawi" data-alternative-spellings="MW">Malawi</option>
                                  <option <?php selected( $country, 'Malaysia' ) ?> value="Malaysia" data-alternative-spellings="MY">Malaysia</option>
                                  <option <?php selected( $country, 'Maldives' ) ?> value="Maldives" data-alternative-spellings="MV">Maldives</option>
                                  <option <?php selected( $country, 'Mali' ) ?> value="Mali" data-alternative-spellings="ML">Mali</option>
                                  <option <?php selected( $country, 'Malta' ) ?> value="Malta" data-alternative-spellings="MT">Malta</option>
                                  <option <?php selected( $country, 'Marshall Islands' ) ?> value="Marshall Islands" data-alternative-spellings="MH" data-relevancy-booster="0.5">Marshall Islands</option>
                                  <option <?php selected( $country, 'Martinique' ) ?> value="Martinique" data-alternative-spellings="MQ">Martinique</option>
                                  <option <?php selected( $country, 'Mauritania' ) ?> value="Mauritania" data-alternative-spellings="MR الموريتانية">Mauritania</option>
                                  <option <?php selected( $country, 'Mauritius' ) ?> value="Mauritius" data-alternative-spellings="MU">Mauritius</option>
                                  <option <?php selected( $country, 'Mayotte' ) ?> value="Mayotte" data-alternative-spellings="YT">Mayotte</option>
                                  <option <?php selected( $country, 'Mexico' ) ?> value="Mexico" data-alternative-spellings="MX Mexicanos" data-relevancy-booster="1.5">Mexico</option>
                                  <option <?php selected( $country, 'Micronesia, Federated States of' ) ?> value="Micronesia, Federated States of" data-alternative-spellings="FM">Micronesia, Federated States of</option>
                                  <option <?php selected( $country, 'Moldova, Republic of' ) ?> value="Moldova, Republic of" data-alternative-spellings="MD">Moldova, Republic of</option>
                                  <option <?php selected( $country, 'Monaco' ) ?> value="Monaco" data-alternative-spellings="MC">Monaco</option>
                                  <option <?php selected( $country, 'Mongolia' ) ?> value="Mongolia" data-alternative-spellings="MN Mongγol ulus Монгол улс">Mongolia</option>
                                  <option <?php selected( $country, 'Montenegro' ) ?> value="Montenegro" data-alternative-spellings="ME">Montenegro</option>
                                  <option <?php selected( $country, 'Montserrat' ) ?> value="Montserrat" data-alternative-spellings="MS" data-relevancy-booster="0.5">Montserrat</option>
                                  <option <?php selected( $country, 'Morocco' ) ?> value="Morocco" data-alternative-spellings="MA المغرب">Morocco</option>
                                  <option <?php selected( $country, 'Mozambique' ) ?> value="Mozambique" data-alternative-spellings="MZ Moçambique">Mozambique</option>
                                  <option <?php selected( $country, 'Myanmar' ) ?> value="Myanmar" data-alternative-spellings="MM">Myanmar</option>
                                  <option <?php selected( $country, 'Namibia' ) ?> value="Namibia" data-alternative-spellings="NA Namibië">Namibia</option>
                                  <option <?php selected( $country, 'Nauru' ) ?> value="Nauru" data-alternative-spellings="NR Naoero" data-relevancy-booster="0.5">Nauru</option>
                                  <option <?php selected( $country, 'Nepal' ) ?> value="Nepal" data-alternative-spellings="NP नेपाल">Nepal</option>
                                  <option <?php selected( $country, 'Netherlands' ) ?> value="Netherlands" data-alternative-spellings="NL Holland Nederland" data-relevancy-booster="1.5">Netherlands</option>
                                  <option <?php selected( $country, 'New Caledonia' ) ?> value="New Caledonia" data-alternative-spellings="NC" data-relevancy-booster="0.5">New Caledonia</option>
                                  <option <?php selected( $country, 'New Zealand' ) ?> value="New Zealand" data-alternative-spellings="NZ Aotearoa">New Zealand</option>
                                  <option <?php selected( $country, 'Nicaragua' ) ?> value="Nicaragua" data-alternative-spellings="NI">Nicaragua</option>
                                  <option <?php selected( $country, 'Niger' ) ?> value="Niger" data-alternative-spellings="NE Nijar">Niger</option>
                                  <option <?php selected( $country, 'Nigeria' ) ?> value="Nigeria" data-alternative-spellings="NG Nijeriya Naíjíríà" data-relevancy-booster="1.5">Nigeria</option>
                                  <option <?php selected( $country, 'Niue' ) ?> value="Niue" data-alternative-spellings="NU" data-relevancy-booster="0.5">Niue</option>
                                  <option <?php selected( $country, 'Norfolk Island' ) ?> value="Norfolk Island" data-alternative-spellings="NF" data-relevancy-booster="0.5">Norfolk Island</option>
                                  <option <?php selected( $country, 'Northern Mariana Islands' ) ?> value="Northern Mariana Islands" data-alternative-spellings="MP" data-relevancy-booster="0.5">Northern Mariana Islands</option>
                                  <option <?php selected( $country, 'Norway' ) ?> value="Norway" data-alternative-spellings="NO Norge Noreg" data-relevancy-booster="1.5">Norway</option>
                                  <option <?php selected( $country, 'Oman' ) ?> value="Oman" data-alternative-spellings="OM عمان">Oman</option>
                                  <option <?php selected( $country, 'Pakistan' ) ?> value="Pakistan" data-alternative-spellings="PK پاکستان" data-relevancy-booster="2">Pakistan</option>
                                  <option <?php selected( $country, 'Palau' ) ?> value="Palau" data-alternative-spellings="PW" data-relevancy-booster="0.5">Palau</option>
                                  <option <?php selected( $country, 'Palestinian Territory, Occupied' ) ?> value="Palestinian Territory, Occupied" data-alternative-spellings="PS فلسطين">Palestinian Territory, Occupied</option>
                                  <option <?php selected( $country, 'Panama' ) ?> value="Panama" data-alternative-spellings="PA">Panama</option>
                                  <option <?php selected( $country, 'Papua New Guinea' ) ?> value="Papua New Guinea" data-alternative-spellings="PG">Papua New Guinea</option>
                                  <option <?php selected( $country, 'Paraguay' ) ?> value="Paraguay" data-alternative-spellings="PY">Paraguay</option>
                                  <option <?php selected( $country, 'Peru' ) ?> value="Peru" data-alternative-spellings="PE">Peru</option>
                                  <option <?php selected( $country, 'Philippines' ) ?> value="Philippines" data-alternative-spellings="PH Pilipinas" data-relevancy-booster="1.5">Philippines</option>
                                  <option <?php selected( $country, 'Pitcairn' ) ?> value="Pitcairn" data-alternative-spellings="PN" data-relevancy-booster="0.5">Pitcairn</option>
                                  <option <?php selected( $country, 'Poland' ) ?> value="Poland" data-alternative-spellings="PL Polska" data-relevancy-booster="1.25">Poland</option>
                                  <option <?php selected( $country, 'Portugal' ) ?> value="Portugal" data-alternative-spellings="PT Portuguesa" data-relevancy-booster="1.5">Portugal</option>
                                  <option <?php selected( $country, 'Puerto Rico' ) ?> value="Puerto Rico" data-alternative-spellings="PR">Puerto Rico</option>
                                  <option <?php selected( $country, 'Qatar' ) ?> value="Qatar" data-alternative-spellings="QA قطر">Qatar</option>
                                  <option <?php selected( $country, 'Réunion' ) ?> value="Réunion" data-alternative-spellings="RE Reunion">Réunion</option>
                                  <option <?php selected( $country, 'Romania' ) ?> value="Romania" data-alternative-spellings="RO Rumania Roumania România">Romania</option>
                                  <option <?php selected( $country, 'Russian Federation' ) ?> value="Russian Federation" data-alternative-spellings="RU Rossiya Российская Россия" data-relevancy-booster="2.5">Russian Federation</option>
                                  <option <?php selected( $country, 'Rwanda' ) ?> value="Rwanda" data-alternative-spellings="RW">Rwanda</option>
                                  <option <?php selected( $country, 'Saint Barthélemy' ) ?> value="Saint Barthélemy" data-alternative-spellings="BL St. Barthelemy">Saint Barthélemy</option>
                                  <option <?php selected( $country, 'Saint Helena' ) ?> value="Saint Helena" data-alternative-spellings="SH St.">Saint Helena</option>
                                  <option <?php selected( $country, 'Saint Kitts and Nevis' ) ?> value="Saint Kitts and Nevis" data-alternative-spellings="KN St.">Saint Kitts and Nevis</option>
                                  <option <?php selected( $country, 'Saint Lucia' ) ?> value="Saint Lucia" data-alternative-spellings="LC St.">Saint Lucia</option>
                                  <option <?php selected( $country, 'Saint Martin (French Part)' ) ?> value="Saint Martin (French Part)" data-alternative-spellings="MF St.">Saint Martin (French Part)</option>
                                  <option <?php selected( $country, 'Saint Pierre and Miquelon' ) ?> value="Saint Pierre and Miquelon" data-alternative-spellings="PM St.">Saint Pierre and Miquelon</option>
                                  <option <?php selected( $country, 'Saint Vincent and the Grenadines' ) ?> value="Saint Vincent and the Grenadines" data-alternative-spellings="VC St.">Saint Vincent and the Grenadines</option>
                                  <option <?php selected( $country, 'Samoa' ) ?> value="Samoa" data-alternative-spellings="WS">Samoa</option>
                                  <option <?php selected( $country, 'San Marino' ) ?> value="San Marino" data-alternative-spellings="SM">San Marino</option>
                                  <option <?php selected( $country, 'Sao Tome and Principe' ) ?> value="Sao Tome and Principe" data-alternative-spellings="ST">Sao Tome and Principe</option>
                                  <option <?php selected( $country, 'Saudi Arabia' ) ?> value="Saudi Arabia" data-alternative-spellings="SA السعودية">Saudi Arabia</option>
                                  <option <?php selected( $country, 'Senegal' ) ?> value="Senegal" data-alternative-spellings="SN Sénégal">Senegal</option>
                                  <option <?php selected( $country, 'Serbia' ) ?> value="Serbia" data-alternative-spellings="RS Србија Srbija">Serbia</option>
                                  <option <?php selected( $country, 'Seychelles' ) ?> value="Seychelles" data-alternative-spellings="SC" data-relevancy-booster="0.5">Seychelles</option>
                                  <option <?php selected( $country, 'Sierra Leone' ) ?> value="Sierra Leone" data-alternative-spellings="SL">Sierra Leone</option>
                                  <option <?php selected( $country, 'Singapore' ) ?> value="Singapore" data-alternative-spellings="SG Singapura  சிங்கப்பூர் குடியரசு 新加坡共和国">Singapore</option>
                                  <option <?php selected( $country, 'Sint Maarten (Dutch Part)' ) ?> value="Sint Maarten (Dutch Part)" data-alternative-spellings="SX">Sint Maarten (Dutch Part)</option>
                                  <option <?php selected( $country, 'Slovakia' ) ?> value="Slovakia" data-alternative-spellings="SK Slovenská Slovensko">Slovakia</option>
                                  <option <?php selected( $country, 'Slovenia' ) ?> value="Slovenia" data-alternative-spellings="SI Slovenija">Slovenia</option>
                                  <option <?php selected( $country, 'Solomon Islands' ) ?> value="Solomon Islands" data-alternative-spellings="SB">Solomon Islands</option>
                                  <option <?php selected( $country, 'Somalia' ) ?> value="Somalia" data-alternative-spellings="SO الصومال">Somalia</option>
                                  <option <?php selected( $country, 'South Africa' ) ?> value="South Africa" data-alternative-spellings="ZA RSA Suid-Afrika">South Africa</option>
                                  <option <?php selected( $country, 'South Georgia and the South Sandwich Islands' ) ?> value="South Georgia and the South Sandwich Islands" data-alternative-spellings="GS">South Georgia and the South Sandwich Islands</option>
                                  <option <?php selected( $country, 'South Sudan' ) ?> value="South Sudan" data-alternative-spellings="SS">South Sudan</option>
                                  <option <?php selected( $country, 'Spain' ) ?> value="Spain" data-alternative-spellings="ES España" data-relevancy-booster="2">Spain</option>
                                  <option <?php selected( $country, 'Sri Lanka' ) ?> value="Sri Lanka" data-alternative-spellings="LK ශ්‍රී ලංකා இலங்கை Ceylon">Sri Lanka</option>
                                  <option <?php selected( $country, 'Sudan' ) ?> value="Sudan" data-alternative-spellings="SD السودان">Sudan</option>
                                  <option <?php selected( $country, 'Suriname' ) ?> value="Suriname" data-alternative-spellings="SR शर्नम् Sarnam Sranangron">Suriname</option>
                                  <option <?php selected( $country, 'Svalbard and Jan Mayen' ) ?> value="Svalbard and Jan Mayen" data-alternative-spellings="SJ" data-relevancy-booster="0.5">Svalbard and Jan Mayen</option>
                                  <option <?php selected( $country, 'Swaziland' ) ?> value="Swaziland" data-alternative-spellings="SZ weSwatini Swatini Ngwane">Swaziland</option>
                                  <option <?php selected( $country, 'Sweden' ) ?> value="Sweden" data-alternative-spellings="SE Sverige" data-relevancy-booster="1.5">Sweden</option>
                                  <option <?php selected( $country, 'Switzerland' ) ?> value="Switzerland" data-alternative-spellings="CH Swiss Confederation Schweiz Suisse Svizzera Svizra" data-relevancy-booster="1.5">Switzerland</option>
                                  <option <?php selected( $country, 'Syrian Arab Republic' ) ?> value="Syrian Arab Republic" data-alternative-spellings="SY Syria سورية">Syrian Arab Republic</option>
                                  <option <?php selected( $country, 'Taiwan' ) ?> value="Taiwan" data-alternative-spellings="TW 台灣 臺灣">Taiwan</option>
                                  <option <?php selected( $country, 'Tajikistan' ) ?> value="Tajikistan" data-alternative-spellings="TJ Тоҷикистон Toçikiston">Tajikistan</option>
                                  <option <?php selected( $country, 'Tanzania, United Republic of' ) ?> value="Tanzania, United Republic of" data-alternative-spellings="TZ">Tanzania, United Republic of</option>
                                  <option <?php selected( $country, 'Thailand' ) ?> value="Thailand" data-alternative-spellings="TH ประเทศไทย Prathet Thai">Thailand</option>
                                  <option <?php selected( $country, 'Timor-Leste' ) ?> value="Timor-Leste" data-alternative-spellings="TL">Timor-Leste</option>
                                  <option <?php selected( $country, 'Togo' ) ?> value="Togo" data-alternative-spellings="TG Togolese">Togo</option>
                                  <option <?php selected( $country, 'Tokelau' ) ?> value="Tokelau" data-alternative-spellings="TK" data-relevancy-booster="0.5">Tokelau</option>
                                  <option <?php selected( $country, 'Tonga' ) ?> value="Tonga" data-alternative-spellings="TO">Tonga</option>
                                  <option <?php selected( $country, 'Trinidad and Tobago' ) ?> value="Trinidad and Tobago" data-alternative-spellings="TT">Trinidad and Tobago</option>
                                  <option <?php selected( $country, 'Tunisia' ) ?> value="Tunisia" data-alternative-spellings="TN تونس">Tunisia</option>
                                  <option <?php selected( $country, 'Turkey' ) ?> value="Turkey" data-alternative-spellings="TR Türkiye Turkiye">Turkey</option>
                                  <option <?php selected( $country, 'Turkmenistan' ) ?> value="Turkmenistan" data-alternative-spellings="TM Türkmenistan">Turkmenistan</option>
                                  <option <?php selected( $country, 'Turks and Caicos Islands' ) ?> value="Turks and Caicos Islands" data-alternative-spellings="TC" data-relevancy-booster="0.5">Turks and Caicos Islands</option>
                                  <option <?php selected( $country, 'Tuvalu' ) ?> value="Tuvalu" data-alternative-spellings="TV" data-relevancy-booster="0.5">Tuvalu</option>
                                  <option <?php selected( $country, 'Uganda' ) ?> value="Uganda" data-alternative-spellings="UG">Uganda</option>
                                  <option <?php selected( $country, 'Ukraine' ) ?> value="Ukraine" data-alternative-spellings="UA Ukrayina Україна">Ukraine</option>
                                  <option <?php selected( $country, 'United Arab Emirates' ) ?> value="United Arab Emirates" data-alternative-spellings="AE UAE الإمارات">United Arab Emirates</option>
                                  <option <?php selected( $country, 'United Kingdom' ) ?> value="United Kingdom" data-alternative-spellings="GB Great Britain England UK Wales Scotland Northern Ireland" data-relevancy-booster="2.5">United Kingdom</option>
                                  <option <?php selected( $country, 'United States' ) ?> value="United States" data-relevancy-booster="3.5" data-alternative-spellings="US USA United States of America">United States</option>
                                  <option <?php selected( $country, 'United States Minor Outlying Islands' ) ?> value="United States Minor Outlying Islands" data-alternative-spellings="UM">United States Minor Outlying Islands</option>
                                  <option <?php selected( $country, 'Uruguay' ) ?> value="Uruguay" data-alternative-spellings="UY">Uruguay</option>
                                  <option <?php selected( $country, 'Uzbekistan' ) ?> value="Uzbekistan" data-alternative-spellings="UZ Ўзбекистон O'zbekstan O‘zbekiston">Uzbekistan</option>
                                  <option <?php selected( $country, 'Vanuatu' ) ?> value="Vanuatu" data-alternative-spellings="VU">Vanuatu</option>
                                  <option <?php selected( $country, 'Venezuela' ) ?> value="Venezuela" data-alternative-spellings="VE">Venezuela</option>
                                  <option <?php selected( $country, 'Vietnam' ) ?> value="Vietnam" data-alternative-spellings="VN Việt Nam" data-relevancy-booster="1.5">Vietnam</option>
                                  <option <?php selected( $country, 'Virgin Islands, British' ) ?> value="Virgin Islands, British" data-alternative-spellings="VG" data-relevancy-booster="0.5">Virgin Islands, British</option>
                                  <option <?php selected( $country, 'Virgin Islands, U.S.' ) ?> value="Virgin Islands, U.S." data-alternative-spellings="VI" data-relevancy-booster="0.5">Virgin Islands, U.S.</option>
                                  <option <?php selected( $country, 'Wallis and Futuna' ) ?> value="Wallis and Futuna" data-alternative-spellings="WF" data-relevancy-booster="0.5">Wallis and Futuna</option>
                                  <option <?php selected( $country, 'Western Sahara' ) ?> value="Western Sahara" data-alternative-spellings="EH لصحراء الغربية">Western Sahara</option>
                                  <option <?php selected( $country, 'Yemen' ) ?> value="Yemen" data-alternative-spellings="YE اليمن">Yemen</option>
                                  <option <?php selected( $country, 'Zambia' ) ?> value="Zambia" data-alternative-spellings="ZM">Zambia</option>
                                  <option <?php selected( $country, 'Zimbabwe' ) ?> value="Zimbabwe" data-alternative-spellings="ZW">Zimbabwe</option> 
                      </select>
          </p>
        </div>
          <!--country end-->

          <!--terms and conditions-->
  		<p>
            <h2>Terms and Conditions</h2>
              <div style="height:120px;width:100%;border:1px solid #ccc;font:11px/11px;overflow:auto;">
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </div>
            <br>
                <input name="rcp_terms_agreement" id="rcp_terms_agreement" class="required" type="checkbox" />
                <label for="rcp_terms_agreement"><?php _e('Agree to our Terms & Conditions', 'rcp'); ?></label>
           <br>

      </p>
  <!--mail chip for wordpress form below to be embeded to  http://aboriginalart.org.au/aaaa/supporter-membership-registration/ and replace Restrict Content Pro form. (this is a plugin) -->
     <!-- <p>
  <label>
    <input type="checkbox" name="mc4wp-subscribe" value="1" />
    Subscribe to our newsletter.  </label>
      </p>-->



      
        <?php

}
add_action( 'rcp_after_password_registration_field', 'pw_rcp_add_user_fields' );
add_action( 'rcp_profile_editor_after', 'pw_rcp_add_user_fields' );

function pippin_rcp_check_for_agreement( $posted ) {
	if( ! isset( $posted['rcp_terms_agreement'] ) ) {
		rcp_errors()->add('agree_to_terms', __('You must agree to our terms of use', 'rcp'), 'register' );

	}
}
add_action('rcp_form_errors', 'pippin_rcp_check_for_agreement');




/**
 * Determines if there are problems with the registration data submitted
 *
 */
function pw_rcp_validate_user_fields_on_register( $posted ) {
  if( empty( $posted['rcp_user_first'] ) ) {
		rcp_errors()->add( 'invalid_first', __( 'Please enter your First Name', 'rcp' ), 'register' );
	}
  if( empty( $posted['rcp_user_last'] ) ) {
		rcp_errors()->add( 'invalid_last', __( 'Please enter your Last Name', 'rcp' ), 'register' );
	}
  
 //supporter-membership-registration')): {
 // if( empty( $posted['rcp_companyname'] ) ) {
//		rcp_errors()->add( 'invalid_companyname', __( 'Please enter your Company Name', 'rcp' ), 'register' );
//	}

  if( empty( $posted['rcp_address1'] ) ) {
		rcp_errors()->add( 'invalid_address1', __( 'Please enter your Address 1', 'rcp' ), 'register' );
	}
  if( empty( $posted['rcp_city'] ) ) {
		rcp_errors()->add( 'invalid_city', __( 'Please enter your City', 'rcp' ), 'register' );
	}
  if( empty( $posted['rcp_postcode'] ) ) {
    rcp_errors()->add( 'invalid_postcode', __( 'Please enter your Postcode', 'rcp' ), 'register' );
  }
  if( empty( $posted['rcp_state'] ) ) {
		rcp_errors()->add( 'invalid_state', __( 'Please enter your State', 'rcp' ), 'register' );
	}
  if( empty( $posted['rcp_country'] ) ) {
		rcp_errors()->add( 'invalid_country', __( 'Select enter your Country', 'rcp' ), 'register' );
	}
}


add_action( 'rcp_form_errors', 'pw_rcp_validate_user_fields_on_register', 10 );


//add edit member screen fields
/**
 *  2 Adds the custom fields to the member edit screen
 *
 */
function pw_rcp_add_member_edit_fields( $user_id = 0 ) {

        $companyname   = get_user_meta( $user_id, 'rcp_companyname', true );
        $telephone   = get_user_meta( $user_id, 'rcp_telephone', true );
        $address1   = get_user_meta( $user_id, 'rcp_address1', true );
        $address2   = get_user_meta( $user_id, 'rcp_address2', true );
        $postcode   = get_user_meta( $user_id, 'rcp_postcode', true );
        $city   = get_user_meta( $user_id, 'rcp_city', true );
        $state   = get_user_meta( $user_id, 'rcp_state', true );
        $country   = get_user_meta( $user_id, 'rcp_country', true );
        $dob   = get_user_meta( $user_id, 'rcp_dob', true );
        $gender   = get_user_meta( $user_id, 'rcp_gender', true );
        $website   = get_user_meta( $user_id, 'rcp_website', true );
        ?>



        <tr valign="top">
               <th scope="row" valign="top">
                      <label for="rcp_dob"><?php _e( 'Date of Birth', 'rcp' ); ?></label>
              </th>
              <td>
                  <input name="rcp_dob" id="rcp_dob" type="text" style="width: 120px;" class="rcp-datepicker hasDatepicker" value="<?php echo esc_attr( $dob ); ?>"/>
                  <p class="description"><?php _e( 'Date of Birth  format of mm-dd-yyyy', 'rcp' ); ?></p>
          </td>
        </tr>
        <tr valign="top">
               <th scope="row" valign="top">
                      <label for="rcp_gender"><?php _e( 'Gender', 'rcp' ); ?></label>
              </th>
              <td>
                  <input name="rcp_gender" id="rcp_gender" type="text" value="<?php echo esc_attr( $gender ); ?>"/>
                  <p class="description"><?php _e( 'Gender Type', 'rcp' ); ?></p>
          </td>
        </tr>
        <tr valign="top">
               <th scope="row" valign="top">
                      <label for="rcp_companyname"><?php _e( 'If Company/Organisation name', 'rcp' ); ?></label>
              </th>
              <td>
                  <input name="rcp_companyname" id="rcp_companyname" type="text" value="<?php echo esc_attr( $companyname ); ?>"/>
                  <p class="description"><?php _e( 'If Company/Organisation name', 'rcp' ); ?></p>
          </td>
        </tr>


         <tr valign="top">
               <th scope="row" valign="top">
                      <label for="rcp_website"><?php _e( 'Website', 'rcp' ); ?></label>
              </th>
              <td>
                  <input name="rcp_website" id="rcp_website" type="text" value="<?php echo esc_attr( $website ); ?>"/>
                  <p class="description"><?php _e( 'e.g. weiv.com.au', 'rcp' ); ?></p>
          </td>
        </tr>

        <tr valign="top">
                <th scope="row" valign="top">
                        <label for="rcp_telephone"><?php _e( 'Telephone add Area code', 'rcp' ); ?></label>
                </th>
                <td>
                        <input name="rcp_telephone" id="rcp_telephone" type="text" value="<?php echo esc_attr( $telephone ); ?>"/>
                        <p class="description"><?php _e( 'Telephone', 'rcp' ); ?></p>
                </td>
        </tr>
        <tr valign="top">
                <th scope="row" valign="top">
                        <label for="rcp_address1"><?php _e( 'Address 1', 'rcp' ); ?></label>
                </th>
                <td>
                        <input name="rcp_address1" id="rcp_address1" type="text" value="<?php echo esc_attr( $address1 ); ?>"/>
                        <p class="description"><?php _e( 'Address 1', 'rcp' ); ?></p>
                </td>
        </tr>
        <tr valign="top">
                <th scope="row" valign="top">
                        <label for="rcp_address2"><?php _e( 'Address 2', 'rcp' ); ?></label>
                </th>
                <td>
                        <input name="rcp_address2" id="rcp_address2" type="text" value="<?php echo esc_attr( $address2 ); ?>"/>
                        <p class="description"><?php _e( 'Address 2', 'rcp' ); ?></p>
                </td>
        </tr>
        <tr valign="top">
                <th scope="row" valign="top">
                        <label for="rcp_postcode"><?php _e( 'Postcode', 'rcp' ); ?></label>
                </th>
                <td>
                        <input name="rcp_postcode" id="rcp_postcode" type="text" value="<?php echo esc_attr( $postcode ); ?>"/>
                        <p class="description"><?php _e( 'Postcode', 'rcp' ); ?></p>
                </td>
        </tr>
        <tr valign="top">
                <th scope="row" valign="top">
                        <label for="rcp_city"><?php _e( 'City', 'rcp' ); ?></label>
                </th>
                <td>
                        <input name="rcp_city" id="rcp_city" type="text" value="<?php echo esc_attr( $city ); ?>"/>
                        <p class="description"><?php _e( 'City', 'rcp' ); ?></p>
                </td>
        </tr>
        <tr valign="top">
                <th scope="row" valign="top">
                        <label for="rcp_state"><?php _e( 'State', 'rcp' ); ?></label>
                </th>
                <td>
                        <input name="rcp_state" id="rcp_state" type="text" value="<?php echo esc_attr( $state); ?>"/>
                        <p class="description"><?php _e( 'State', 'rcp' ); ?></p>
                </td>
        </tr>
        <tr valign="top">
               <th scope="row" valign="top">
                  <label for="rcp_country"><?php _e( 'Country', 'rcp' ); ?></label>
               </th>
               <td>
                 <div class="country-styled-select">
                   <p id="rcp_country_wrap">
                           <!--<label for="rcp_country">Select Country</label>-->

                           <?php
                           //get dropdown saved value
                           $selected = get_the_author_meta( 'rcp_country', $user->ID );
                           ?>
                               <select name="rcp_country" id="rcp_country" class="required" autocorrect="off" autocomplete="off">
                                  <option value="Choose"></option>
                                  <option <?php selected( $country, 'Australia' ) ?> value="Australia" data-alternative-spellings="AU" data-relevancy-booster="1.5">Australia</option>
                                  <option <?php selected( $country, 'Åland Islands' ) ?> value="Åland Islands" data-alternative-spellings="AX Aaland Aland" data-relevancy-booster="0.5">Åland Islands</option>
                                  <option <?php selected( $country, 'Albania' ) ?> value="Albania" data-alternative-spellings="AL">Albania</option>
                                  <option <?php selected( $country, 'Algeria' ) ?> value="Algeria" data-alternative-spellings="DZ الجزائر">Algeria</option>
                                  <option <?php selected( $country, 'American Samoa' ) ?> value="American Samoa" data-alternative-spellings="AS" data-relevancy-booster="0.5">American Samoa</option>
                                  <option <?php selected( $country, 'Andorra' ) ?> value="Andorra" data-alternative-spellings="AD" data-relevancy-booster="0.5">Andorra</option>
                                  <option <?php selected( $country, 'Angola' ) ?> value="Angola" data-alternative-spellings="AO">Angola</option>
                                  <option <?php selected( $country, 'Anguilla' ) ?> value="Anguilla" data-alternative-spellings="AI" data-relevancy-booster="0.5">Anguilla</option>
                                  <option <?php selected( $country, 'Antarctica' ) ?> value="Antarctica" data-alternative-spellings="AQ" data-relevancy-booster="0.5">Antarctica</option>
                                  <option <?php selected( $country, 'Antigua And Barbuda' ) ?> value="Antigua And Barbuda" data-alternative-spellings="AG" data-relevancy-booster="0.5">Antigua And Barbuda</option>
                                  <option <?php selected( $country, 'Argentina' ) ?> value="Argentina" data-alternative-spellings="AR">Argentina</option>
                                  <option <?php selected( $country, 'Armenia' ) ?> value="Armenia" data-alternative-spellings="AM Հայաստան">Armenia</option>
                                  <option <?php selected( $country, 'Aruba' ) ?> value="Aruba" data-alternative-spellings="AW" data-relevancy-booster="0.5">Aruba</option>
                                  <option <?php selected( $country, 'Australia' ) ?> value="Australia" data-alternative-spellings="AU" data-relevancy-booster="1.5">Australia</option>
                                  <option <?php selected( $country, 'Austria' ) ?> value="Austria" data-alternative-spellings="AT Österreich Osterreich Oesterreich ">Austria</option>
                                  <option <?php selected( $country, 'Azerbaijan' ) ?> value="Azerbaijan" data-alternative-spellings="AZ">Azerbaijan</option>
                                  <option <?php selected( $country, 'Bahamas' ) ?> value="Bahamas" data-alternative-spellings="BS">Bahamas</option>
                                  <option <?php selected( $country, 'Bahrain' ) ?> value="Bahrain" data-alternative-spellings="BH البحرين">Bahrain</option>
                                  <option <?php selected( $country, 'Bangladesh' ) ?> value="Bangladesh" data-alternative-spellings="BD বাংলাদেশ" data-relevancy-booster="2">Bangladesh</option>
                                  <option <?php selected( $country, 'Barbados' ) ?> value="Barbados" data-alternative-spellings="BB">Barbados</option>
                                  <option <?php selected( $country, 'Belarus' ) ?> value="Belarus" data-alternative-spellings="BY Беларусь">Belarus</option>
                                  <option <?php selected( $country, 'Belgium' ) ?> value="Belgium" data-alternative-spellings="BE België Belgie Belgien Belgique" data-relevancy-booster="1.5">Belgium</option>
                                  <option <?php selected( $country, 'Belize' ) ?> value="Belize" data-alternative-spellings="BZ">Belize</option>
                                  <option <?php selected( $country, 'Benin' ) ?> value="Benin" data-alternative-spellings="BJ">Benin</option>
                                  <option <?php selected( $country, 'Bermuda' ) ?> value="Bermuda" data-alternative-spellings="BM" data-relevancy-booster="0.5">Bermuda</option>
                                  <option <?php selected( $country, 'Bhutan' ) ?> value="Bhutan" data-alternative-spellings="BT भूटान">Bhutan</option>
                                  <option <?php selected( $country, 'Bolivia' ) ?> value="Bolivia" data-alternative-spellings="BO">Bolivia</option>
                                  <option <?php selected( $country, 'Bonaire, Sint Eustatius and Saba' ) ?> value="Bonaire, Sint Eustatius and Saba" data-alternative-spellings="BQ">Bonaire, Sint Eustatius and Saba</option>
                                  <option <?php selected( $country, 'Bosnia and Herzegovina' ) ?> value="Bosnia and Herzegovina" data-alternative-spellings="BA Босна и Херцеговина">Bosnia and Herzegovina</option>
                                  <option <?php selected( $country, 'Botswana' ) ?> value="Botswana" data-alternative-spellings="BW">Botswana</option>
                                  <option <?php selected( $country, 'Bouvet Island' ) ?> value="Bouvet Island" data-alternative-spellings="BV">Bouvet Island</option>
                                  <option <?php selected( $country, 'Brazil' ) ?> value="Brazil" data-alternative-spellings="BR Brasil" data-relevancy-booster="2">Brazil</option>
                                  <option <?php selected( $country, 'British Indian Ocean Territory' ) ?> value="British Indian Ocean Territory" data-alternative-spellings="IO">British Indian Ocean Territory</option>
                                  <option <?php selected( $country, 'Brunei Darussalam' ) ?> value="Brunei Darussalam" data-alternative-spellings="BN">Brunei Darussalam</option>
                                  <option <?php selected( $country, 'Bulgaria' ) ?> value="Bulgaria" data-alternative-spellings="BG България">Bulgaria</option>
                                  <option <?php selected( $country, 'Burkina Faso' ) ?> value="Burkina Faso" data-alternative-spellings="BF">Burkina Faso</option>
                                  <option <?php selected( $country, 'Burundi' ) ?> value="Burundi" data-alternative-spellings="BI">Burundi</option>
                                  <option <?php selected( $country, 'Cambodia' ) ?> value="Cambodia" data-alternative-spellings="KH កម្ពុជា">Cambodia</option>
                                  <option <?php selected( $country, 'Cameroon' ) ?> value="Cameroon" data-alternative-spellings="CM">Cameroon</option>
                                  <option <?php selected( $country, 'Canada' ) ?> value="Canada" data-alternative-spellings="CA" data-relevancy-booster="2">Canada</option>
                                  <option <?php selected( $country, 'Cape Verde' ) ?> value="Cape Verde" data-alternative-spellings="CV Cabo">Cape Verde</option>
                                  <option <?php selected( $country, 'Cayman Islands' ) ?> value="Cayman Islands" data-alternative-spellings="KY" data-relevancy-booster="0.5">Cayman Islands</option>
                                  <option <?php selected( $country, 'Central African Republic' ) ?> value="Central African Republic" data-alternative-spellings="CF">Central African Republic</option>
                                  <option <?php selected( $country, 'Chad' ) ?> value="Chad" data-alternative-spellings="TD تشاد‎ Tchad">Chad</option>
                                  <option <?php selected( $country, 'Chile' ) ?> value="Chile" data-alternative-spellings="CL">Chile</option>
                                  <option <?php selected( $country, 'China' ) ?> value="China" data-relevancy-booster="3.5" data-alternative-spellings="CN Zhongguo Zhonghua Peoples Republic 中国/中华">China</option>
                                  <option <?php selected( $country, 'Christmas Island' ) ?> value="Christmas Island" data-alternative-spellings="CX" data-relevancy-booster="0.5">Christmas Island</option>
                                  <option <?php selected( $country, 'Cocos (Keeling) Islands' ) ?> value="Cocos (Keeling) Islands" data-alternative-spellings="CC" data-relevancy-booster="0.5">Cocos (Keeling) Islands</option>
                                  <option <?php selected( $country, 'Colombia' ) ?> value="Colombia" data-alternative-spellings="CO">Colombia</option>
                                  <option <?php selected( $country, 'Comoros' ) ?> value="Comoros" data-alternative-spellings="KM جزر القمر">Comoros</option>
                                  <option <?php selected( $country, 'Congo' ) ?> value="Congo" data-alternative-spellings="CG">Congo</option>
                                  <option <?php selected( $country, 'Congo, the Democratic Republic of the' ) ?> value="Congo, the Democratic Republic of the" data-alternative-spellings="CD Congo-Brazzaville Repubilika ya Kongo">Congo, the Democratic Republic of the</option>
                                  <option <?php selected( $country, 'Cook Islands' ) ?> value="Cook Islands" data-alternative-spellings="CK" data-relevancy-booster="0.5">Cook Islands</option>
                                  <option <?php selected( $country, 'Costa Rica' ) ?> value="Costa Rica" data-alternative-spellings="CR">Costa Rica</option>
                                  <option <?php selected( $country, "Côte d'Ivoire" ) ?> value="Côte d'Ivoire" data-alternative-spellings="CI Cote dIvoire">Côte d'Ivoire</option>
                                  <option <?php selected( $country, 'Croatia' ) ?> value="Croatia" data-alternative-spellings="HR Hrvatska">Croatia</option>
                                  <option <?php selected( $country, 'Cuba' ) ?> value="Cuba" data-alternative-spellings="CU">Cuba</option>
                                  <option <?php selected( $country, 'Curaçao' ) ?> value="Curaçao" data-alternative-spellings="CW Curacao">Curaçao</option>
                                  <option <?php selected( $country, 'Cyprus' ) ?> value="Cyprus" data-alternative-spellings="CY Κύπρος Kýpros Kıbrıs">Cyprus</option>
                                  <option <?php selected( $country, 'Czech Republic' ) ?> value="Czech Republic" data-alternative-spellings="CZ Česká Ceska">Czech Republic</option>
                                  <option <?php selected( $country, 'Denmark' ) ?> value="Denmark" data-alternative-spellings="DK Danmark" data-relevancy-booster="1.5">Denmark</option>
                                  <option <?php selected( $country, 'Djibouti' ) ?> value="Djibouti" data-alternative-spellings="DJ جيبوتي‎ Jabuuti Gabuuti">Djibouti</option>
                                  <option <?php selected( $country, 'Dominica' ) ?> value="Dominica" data-alternative-spellings="DM Dominique" data-relevancy-booster="0.5">Dominica</option>
                                  <option <?php selected( $country, 'Dominican Republic' ) ?> value="Dominican Republic" data-alternative-spellings="DO">Dominican Republic</option>
                                  <option <?php selected( $country, 'Ecuador' ) ?> value="Ecuador" data-alternative-spellings="EC">Ecuador</option>
                                  <option <?php selected( $country, 'Egypt' ) ?> value="Egypt" data-alternative-spellings="EG" data-relevancy-booster="1.5">Egypt</option>
                                  <option <?php selected( $country, 'El Salvador' ) ?> value="El Salvador" data-alternative-spellings="SV">El Salvador</option>
                                  <option <?php selected( $country, 'Equatorial Guinea' ) ?> value="Equatorial Guinea" data-alternative-spellings="GQ">Equatorial Guinea</option>
                                  <option <?php selected( $country, 'Eritrea' ) ?> value="Eritrea" data-alternative-spellings="ER إرتريا ኤርትራ">Eritrea</option>
                                  <option <?php selected( $country, 'Estonia' ) ?> value="Estonia" data-alternative-spellings="EE Eesti">Estonia</option>
                                  <option <?php selected( $country, 'Ethiopia' ) ?> value="Ethiopia" data-alternative-spellings="ET ኢትዮጵያ">Ethiopia</option>
                                  <option <?php selected( $country, 'Falkland Islands (Malvinas)' ) ?> value="Falkland Islands (Malvinas)" data-alternative-spellings="FK" data-relevancy-booster="0.5">Falkland Islands (Malvinas)</option>
                                  <option <?php selected( $country, 'Faroe Islands' ) ?> value="Faroe Islands" data-alternative-spellings="FO Føroyar Færøerne" data-relevancy-booster="0.5">Faroe Islands</option>
                                  <option <?php selected( $country, 'Fiji' ) ?> value="Fiji" data-alternative-spellings="FJ Viti फ़िजी">Fiji</option>
                                  <option <?php selected( $country, 'Finland' ) ?> value="Finland" data-alternative-spellings="FI Suomi">Finland</option>
                                  <option <?php selected( $country, 'France' ) ?> value="France" data-alternative-spellings="FR République française" data-relevancy-booster="2.5">France</option>
                                  <option <?php selected( $country, 'French Guiana' ) ?> value="French Guiana" data-alternative-spellings="GF">French Guiana</option>
                                  <option <?php selected( $country, 'French Polynesia' ) ?> value="French Polynesia" data-alternative-spellings="PF Polynésie française">French Polynesia</option>
                                  <option <?php selected( $country, 'French Southern Territories' ) ?> value="French Southern Territories" data-alternative-spellings="TF">French Southern Territories</option>
                                  <option <?php selected( $country, 'Gabon' ) ?> value="Gabon" data-alternative-spellings="GA République Gabonaise">Gabon</option>
                                  <option <?php selected( $country, 'Gambia' ) ?> value="Gambia" data-alternative-spellings="GM">Gambia</option>
                                  <option <?php selected( $country, 'Georgia' ) ?> value="Georgia" data-alternative-spellings="GE საქართველო">Georgia</option>
                                  <option <?php selected( $country, 'Germany' ) ?> value="Germany" data-alternative-spellings="DE Bundesrepublik Deutschland" data-relevancy-booster="3">Germany</option>
                                  <option <?php selected( $country, 'Ghana' ) ?> value="Ghana" data-alternative-spellings="GH">Ghana</option>
                                  <option <?php selected( $country, 'Gibraltar' ) ?> value="Gibraltar" data-alternative-spellings="GI" data-relevancy-booster="0.5">Gibraltar</option>
                                  <option <?php selected( $country, 'Greece' ) ?> value="Greece" data-alternative-spellings="GR Ελλάδα" data-relevancy-booster="1.5">Greece</option>
                                  <option <?php selected( $country, 'Greenland' ) ?> value="Greenland" data-alternative-spellings="GL grønland" data-relevancy-booster="0.5">Greenland</option>
                                  <option <?php selected( $country, 'Grenada' ) ?> value="Grenada" data-alternative-spellings="GD">Grenada</option>
                                  <option <?php selected( $country, 'Guadeloupe' ) ?> value="Guadeloupe" data-alternative-spellings="GP">Guadeloupe</option>
                                  <option <?php selected( $country, 'Guam' ) ?> value="Guam" data-alternative-spellings="GU">Guam</option>
                                  <option <?php selected( $country, 'Guatemala' ) ?> value="Guatemala" data-alternative-spellings="GT">Guatemala</option>
                                  <option <?php selected( $country, 'Guernsey' ) ?> value="Guernsey" data-alternative-spellings="GG" data-relevancy-booster="0.5">Guernsey</option>
                                  <option <?php selected( $country, 'Guinea' ) ?> value="Guinea" data-alternative-spellings="GN">Guinea</option>
                                  <option <?php selected( $country, 'Guinea-Bissau' ) ?> value="Guinea-Bissau" data-alternative-spellings="GW">Guinea-Bissau</option>
                                  <option <?php selected( $country, 'Guyana' ) ?> value="Guyana" data-alternative-spellings="GY">Guyana</option>
                                  <option <?php selected( $country, 'Haiti' ) ?> value="Haiti" data-alternative-spellings="HT">Haiti</option>
                                  <option <?php selected( $country, 'Heard Island and McDonald Islands' ) ?> value="Heard Island and McDonald Islands" data-alternative-spellings="HM">Heard Island and McDonald Islands</option>
                                  <option <?php selected( $country, 'Holy See (Vatican City State)' ) ?> value="Holy See (Vatican City State)" data-alternative-spellings="VA" data-relevancy-booster="0.5">Holy See (Vatican City State)</option>
                                  <option <?php selected( $country, 'Honduras' ) ?> value="Honduras" data-alternative-spellings="HN">Honduras</option>
                                  <option <?php selected( $country, 'Hong Kong' ) ?> value="Hong Kong" data-alternative-spellings="HK 香港">Hong Kong</option>
                                  <option <?php selected( $country, 'Hungary' ) ?> value="Hungary" data-alternative-spellings="HU Magyarország">Hungary</option>
                                  <option <?php selected( $country, 'Iceland' ) ?> value="Iceland" data-alternative-spellings="IS Island">Iceland</option>
                                  <option <?php selected( $country, 'India' ) ?> value="India" data-alternative-spellings="IN भारत गणराज्य Hindustan" data-relevancy-booster="3">India</option>
                                  <option <?php selected( $country, 'Indonesia' ) ?> value="Indonesia" data-alternative-spellings="ID" data-relevancy-booster="2">Indonesia</option>
                                  <option <?php selected( $country, 'Iran, Islamic Republic of' ) ?> value="Iran, Islamic Republic of" data-alternative-spellings="IR ایران">Iran, Islamic Republic of</option>
                                  <option <?php selected( $country, 'Iraq' ) ?> value="Iraq" data-alternative-spellings="IQ العراق‎">Iraq</option>
                                  <option <?php selected( $country, 'Ireland' ) ?> value="Ireland" data-alternative-spellings="IE Éire" data-relevancy-booster="1.2">Ireland</option>
                                  <option <?php selected( $country, 'Isle of Man' ) ?> value="Isle of Man" data-alternative-spellings="IM" data-relevancy-booster="0.5">Isle of Man</option>
                                  <option <?php selected( $country, 'Israel' ) ?> value="Israel" data-alternative-spellings="IL إسرائيل ישראל">Israel</option>
                                  <option <?php selected( $country, 'Italy' ) ?> value="Italy" data-alternative-spellings="IT Italia" data-relevancy-booster="2">Italy</option>
                                  <option <?php selected( $country, 'Jamaica' ) ?> value="Jamaica" data-alternative-spellings="JM">Jamaica</option>
                                  <option <?php selected( $country, 'Japan' ) ?> value="Japan" data-alternative-spellings="JP Nippon Nihon 日本" data-relevancy-booster="2.5">Japan</option>
                                  <option <?php selected( $country, 'Jersey' ) ?> value="Jersey" data-alternative-spellings="JE" data-relevancy-booster="0.5">Jersey</option>
                                  <option <?php selected( $country, 'Jordan' ) ?> value="Jordan" data-alternative-spellings="JO الأردن">Jordan</option>
                                  <option <?php selected( $country, 'Kazakhstan' ) ?> value="Kazakhstan" data-alternative-spellings="KZ Қазақстан Казахстан">Kazakhstan</option>
                                  <option <?php selected( $country, 'Kenya' ) ?> value="Kenya" data-alternative-spellings="KE">Kenya</option>
                                  <option <?php selected( $country, 'Kiribati' ) ?> value="Kiribati" data-alternative-spellings="KI">Kiribati</option>
                                  <option <?php selected( $country, "Korea, Democratic People's Republic of" ) ?> value="Korea, Democratic People's Republic of" data-alternative-spellings="KP North Korea">Korea, Democratic People's Republic of</option>
                                  <option <?php selected( $country, 'Korea, Republic of' ) ?> value="Korea, Republic of" data-alternative-spellings="KR South Korea" data-relevancy-booster="1.5">Korea, Republic of</option>
                                  <option <?php selected( $country, 'Kuwait' ) ?> value="Kuwait" data-alternative-spellings="KW الكويت">Kuwait</option>
                                  <option <?php selected( $country, 'Kyrgyzstan' ) ?> value="Kyrgyzstan" data-alternative-spellings="KG Кыргызстан">Kyrgyzstan</option>
                                  <option <?php selected( $country, "Lao People's Democratic Republic" ) ?> value="Lao People's Democratic Republic" data-alternative-spellings="LA">Lao People's Democratic Republic</option>
                                  <option <?php selected( $country, 'Latvia' ) ?> value="Latvia" data-alternative-spellings="LV Latvija">Latvia</option>
                                  <option <?php selected( $country, 'Lebanon' ) ?> value="Lebanon" data-alternative-spellings="LB لبنان">Lebanon</option>
                                  <option <?php selected( $country, 'Lesotho' ) ?> value="Lesotho" data-alternative-spellings="LS">Lesotho</option>
                                  <option <?php selected( $country, 'Liberia' ) ?> value="Liberia" data-alternative-spellings="LR">Liberia</option>
                                  <option <?php selected( $country, 'Libyan Arab Jamahiriya' ) ?> value="Libyan Arab Jamahiriya" data-alternative-spellings="LY ليبيا">Libyan Arab Jamahiriya</option>
                                  <option <?php selected( $country, 'Liechtenstein' ) ?> value="Liechtenstein" data-alternative-spellings="LI">Liechtenstein</option>
                                  <option <?php selected( $country, 'Lithuania' ) ?> value="Lithuania" data-alternative-spellings="LT Lietuva">Lithuania</option>
                                  <option <?php selected( $country, 'Luxembourg' ) ?> value="Luxembourg" data-alternative-spellings="LU">Luxembourg</option>
                                  <option <?php selected( $country, 'Macao' ) ?> value="Macao" data-alternative-spellings="MO">Macao</option>
                                  <option <?php selected( $country, 'Macedonia, The Former Yugoslav Republic Of' ) ?> value="Macedonia, The Former Yugoslav Republic Of" data-alternative-spellings="MK Македонија">Macedonia, The Former Yugoslav Republic Of</option>
                                  <option <?php selected( $country, 'Madagascar' ) ?> value="Madagascar" data-alternative-spellings="MG Madagasikara">Madagascar</option>
                                  <option <?php selected( $country, 'Malawi' ) ?> value="Malawi" data-alternative-spellings="MW">Malawi</option>
                                  <option <?php selected( $country, 'Malaysia' ) ?> value="Malaysia" data-alternative-spellings="MY">Malaysia</option>
                                  <option <?php selected( $country, 'Maldives' ) ?> value="Maldives" data-alternative-spellings="MV">Maldives</option>
                                  <option <?php selected( $country, 'Mali' ) ?> value="Mali" data-alternative-spellings="ML">Mali</option>
                                  <option <?php selected( $country, 'Malta' ) ?> value="Malta" data-alternative-spellings="MT">Malta</option>
                                  <option <?php selected( $country, 'Marshall Islands' ) ?> value="Marshall Islands" data-alternative-spellings="MH" data-relevancy-booster="0.5">Marshall Islands</option>
                                  <option <?php selected( $country, 'Martinique' ) ?> value="Martinique" data-alternative-spellings="MQ">Martinique</option>
                                  <option <?php selected( $country, 'Mauritania' ) ?> value="Mauritania" data-alternative-spellings="MR الموريتانية">Mauritania</option>
                                  <option <?php selected( $country, 'Mauritius' ) ?> value="Mauritius" data-alternative-spellings="MU">Mauritius</option>
                                  <option <?php selected( $country, 'Mayotte' ) ?> value="Mayotte" data-alternative-spellings="YT">Mayotte</option>
                                  <option <?php selected( $country, 'Mexico' ) ?> value="Mexico" data-alternative-spellings="MX Mexicanos" data-relevancy-booster="1.5">Mexico</option>
                                  <option <?php selected( $country, 'Micronesia, Federated States of' ) ?> value="Micronesia, Federated States of" data-alternative-spellings="FM">Micronesia, Federated States of</option>
                                  <option <?php selected( $country, 'Moldova, Republic of' ) ?> value="Moldova, Republic of" data-alternative-spellings="MD">Moldova, Republic of</option>
                                  <option <?php selected( $country, 'Monaco' ) ?> value="Monaco" data-alternative-spellings="MC">Monaco</option>
                                  <option <?php selected( $country, 'Mongolia' ) ?> value="Mongolia" data-alternative-spellings="MN Mongγol ulus Монгол улс">Mongolia</option>
                                  <option <?php selected( $country, 'Montenegro' ) ?> value="Montenegro" data-alternative-spellings="ME">Montenegro</option>
                                  <option <?php selected( $country, 'Montserrat' ) ?> value="Montserrat" data-alternative-spellings="MS" data-relevancy-booster="0.5">Montserrat</option>
                                  <option <?php selected( $country, 'Morocco' ) ?> value="Morocco" data-alternative-spellings="MA المغرب">Morocco</option>
                                  <option <?php selected( $country, 'Mozambique' ) ?> value="Mozambique" data-alternative-spellings="MZ Moçambique">Mozambique</option>
                                  <option <?php selected( $country, 'Myanmar' ) ?> value="Myanmar" data-alternative-spellings="MM">Myanmar</option>
                                  <option <?php selected( $country, 'Namibia' ) ?> value="Namibia" data-alternative-spellings="NA Namibië">Namibia</option>
                                  <option <?php selected( $country, 'Nauru' ) ?> value="Nauru" data-alternative-spellings="NR Naoero" data-relevancy-booster="0.5">Nauru</option>
                                  <option <?php selected( $country, 'Nepal' ) ?> value="Nepal" data-alternative-spellings="NP नेपाल">Nepal</option>
                                  <option <?php selected( $country, 'Netherlands' ) ?> value="Netherlands" data-alternative-spellings="NL Holland Nederland" data-relevancy-booster="1.5">Netherlands</option>
                                  <option <?php selected( $country, 'New Caledonia' ) ?> value="New Caledonia" data-alternative-spellings="NC" data-relevancy-booster="0.5">New Caledonia</option>
                                  <option <?php selected( $country, 'New Zealand' ) ?> value="New Zealand" data-alternative-spellings="NZ Aotearoa">New Zealand</option>
                                  <option <?php selected( $country, 'Nicaragua' ) ?> value="Nicaragua" data-alternative-spellings="NI">Nicaragua</option>
                                  <option <?php selected( $country, 'Niger' ) ?> value="Niger" data-alternative-spellings="NE Nijar">Niger</option>
                                  <option <?php selected( $country, 'Nigeria' ) ?> value="Nigeria" data-alternative-spellings="NG Nijeriya Naíjíríà" data-relevancy-booster="1.5">Nigeria</option>
                                  <option <?php selected( $country, 'Niue' ) ?> value="Niue" data-alternative-spellings="NU" data-relevancy-booster="0.5">Niue</option>
                                  <option <?php selected( $country, 'Norfolk Island' ) ?> value="Norfolk Island" data-alternative-spellings="NF" data-relevancy-booster="0.5">Norfolk Island</option>
                                  <option <?php selected( $country, 'Northern Mariana Islands' ) ?> value="Northern Mariana Islands" data-alternative-spellings="MP" data-relevancy-booster="0.5">Northern Mariana Islands</option>
                                  <option <?php selected( $country, 'Norway' ) ?> value="Norway" data-alternative-spellings="NO Norge Noreg" data-relevancy-booster="1.5">Norway</option>
                                  <option <?php selected( $country, 'Oman' ) ?> value="Oman" data-alternative-spellings="OM عمان">Oman</option>
                                  <option <?php selected( $country, 'Pakistan' ) ?> value="Pakistan" data-alternative-spellings="PK پاکستان" data-relevancy-booster="2">Pakistan</option>
                                  <option <?php selected( $country, 'Palau' ) ?> value="Palau" data-alternative-spellings="PW" data-relevancy-booster="0.5">Palau</option>
                                  <option <?php selected( $country, 'Palestinian Territory, Occupied' ) ?> value="Palestinian Territory, Occupied" data-alternative-spellings="PS فلسطين">Palestinian Territory, Occupied</option>
                                  <option <?php selected( $country, 'Panama' ) ?> value="Panama" data-alternative-spellings="PA">Panama</option>
                                  <option <?php selected( $country, 'Papua New Guinea' ) ?> value="Papua New Guinea" data-alternative-spellings="PG">Papua New Guinea</option>
                                  <option <?php selected( $country, 'Paraguay' ) ?> value="Paraguay" data-alternative-spellings="PY">Paraguay</option>
                                  <option <?php selected( $country, 'Peru' ) ?> value="Peru" data-alternative-spellings="PE">Peru</option>
                                  <option <?php selected( $country, 'Philippines' ) ?> value="Philippines" data-alternative-spellings="PH Pilipinas" data-relevancy-booster="1.5">Philippines</option>
                                  <option <?php selected( $country, 'Pitcairn' ) ?> value="Pitcairn" data-alternative-spellings="PN" data-relevancy-booster="0.5">Pitcairn</option>
                                  <option <?php selected( $country, 'Poland' ) ?> value="Poland" data-alternative-spellings="PL Polska" data-relevancy-booster="1.25">Poland</option>
                                  <option <?php selected( $country, 'Portugal' ) ?> value="Portugal" data-alternative-spellings="PT Portuguesa" data-relevancy-booster="1.5">Portugal</option>
                                  <option <?php selected( $country, 'Puerto Rico' ) ?> value="Puerto Rico" data-alternative-spellings="PR">Puerto Rico</option>
                                  <option <?php selected( $country, 'Qatar' ) ?> value="Qatar" data-alternative-spellings="QA قطر">Qatar</option>
                                  <option <?php selected( $country, 'Réunion' ) ?> value="Réunion" data-alternative-spellings="RE Reunion">Réunion</option>
                                  <option <?php selected( $country, 'Romania' ) ?> value="Romania" data-alternative-spellings="RO Rumania Roumania România">Romania</option>
                                  <option <?php selected( $country, 'Russian Federation' ) ?> value="Russian Federation" data-alternative-spellings="RU Rossiya Российская Россия" data-relevancy-booster="2.5">Russian Federation</option>
                                  <option <?php selected( $country, 'Rwanda' ) ?> value="Rwanda" data-alternative-spellings="RW">Rwanda</option>
                                  <option <?php selected( $country, 'Saint Barthélemy' ) ?> value="Saint Barthélemy" data-alternative-spellings="BL St. Barthelemy">Saint Barthélemy</option>
                                  <option <?php selected( $country, 'Saint Helena' ) ?> value="Saint Helena" data-alternative-spellings="SH St.">Saint Helena</option>
                                  <option <?php selected( $country, 'Saint Kitts and Nevis' ) ?> value="Saint Kitts and Nevis" data-alternative-spellings="KN St.">Saint Kitts and Nevis</option>
                                  <option <?php selected( $country, 'Saint Lucia' ) ?> value="Saint Lucia" data-alternative-spellings="LC St.">Saint Lucia</option>
                                  <option <?php selected( $country, 'Saint Martin (French Part)' ) ?> value="Saint Martin (French Part)" data-alternative-spellings="MF St.">Saint Martin (French Part)</option>
                                  <option <?php selected( $country, 'Saint Pierre and Miquelon' ) ?> value="Saint Pierre and Miquelon" data-alternative-spellings="PM St.">Saint Pierre and Miquelon</option>
                                  <option <?php selected( $country, 'Saint Vincent and the Grenadines' ) ?> value="Saint Vincent and the Grenadines" data-alternative-spellings="VC St.">Saint Vincent and the Grenadines</option>
                                  <option <?php selected( $country, 'Samoa' ) ?> value="Samoa" data-alternative-spellings="WS">Samoa</option>
                                  <option <?php selected( $country, 'San Marino' ) ?> value="San Marino" data-alternative-spellings="SM">San Marino</option>
                                  <option <?php selected( $country, 'Sao Tome and Principe' ) ?> value="Sao Tome and Principe" data-alternative-spellings="ST">Sao Tome and Principe</option>
                                  <option <?php selected( $country, 'Saudi Arabia' ) ?> value="Saudi Arabia" data-alternative-spellings="SA السعودية">Saudi Arabia</option>
                                  <option <?php selected( $country, 'Senegal' ) ?> value="Senegal" data-alternative-spellings="SN Sénégal">Senegal</option>
                                  <option <?php selected( $country, 'Serbia' ) ?> value="Serbia" data-alternative-spellings="RS Србија Srbija">Serbia</option>
                                  <option <?php selected( $country, 'Seychelles' ) ?> value="Seychelles" data-alternative-spellings="SC" data-relevancy-booster="0.5">Seychelles</option>
                                  <option <?php selected( $country, 'Sierra Leone' ) ?> value="Sierra Leone" data-alternative-spellings="SL">Sierra Leone</option>
                                  <option <?php selected( $country, 'Singapore' ) ?> value="Singapore" data-alternative-spellings="SG Singapura  சிங்கப்பூர் குடியரசு 新加坡共和国">Singapore</option>
                                  <option <?php selected( $country, 'Sint Maarten (Dutch Part)' ) ?> value="Sint Maarten (Dutch Part)" data-alternative-spellings="SX">Sint Maarten (Dutch Part)</option>
                                  <option <?php selected( $country, 'Slovakia' ) ?> value="Slovakia" data-alternative-spellings="SK Slovenská Slovensko">Slovakia</option>
                                  <option <?php selected( $country, 'Slovenia' ) ?> value="Slovenia" data-alternative-spellings="SI Slovenija">Slovenia</option>
                                  <option <?php selected( $country, 'Solomon Islands' ) ?> value="Solomon Islands" data-alternative-spellings="SB">Solomon Islands</option>
                                  <option <?php selected( $country, 'Somalia' ) ?> value="Somalia" data-alternative-spellings="SO الصومال">Somalia</option>
                                  <option <?php selected( $country, 'South Africa' ) ?> value="South Africa" data-alternative-spellings="ZA RSA Suid-Afrika">South Africa</option>
                                  <option <?php selected( $country, 'South Georgia and the South Sandwich Islands' ) ?> value="South Georgia and the South Sandwich Islands" data-alternative-spellings="GS">South Georgia and the South Sandwich Islands</option>
                                  <option <?php selected( $country, 'South Sudan' ) ?> value="South Sudan" data-alternative-spellings="SS">South Sudan</option>
                                  <option <?php selected( $country, 'Spain' ) ?> value="Spain" data-alternative-spellings="ES España" data-relevancy-booster="2">Spain</option>
                                  <option <?php selected( $country, 'Sri Lanka' ) ?> value="Sri Lanka" data-alternative-spellings="LK ශ්‍රී ලංකා இலங்கை Ceylon">Sri Lanka</option>
                                  <option <?php selected( $country, 'Sudan' ) ?> value="Sudan" data-alternative-spellings="SD السودان">Sudan</option>
                                  <option <?php selected( $country, 'Suriname' ) ?> value="Suriname" data-alternative-spellings="SR शर्नम् Sarnam Sranangron">Suriname</option>
                                  <option <?php selected( $country, 'Svalbard and Jan Mayen' ) ?> value="Svalbard and Jan Mayen" data-alternative-spellings="SJ" data-relevancy-booster="0.5">Svalbard and Jan Mayen</option>
                                  <option <?php selected( $country, 'Swaziland' ) ?> value="Swaziland" data-alternative-spellings="SZ weSwatini Swatini Ngwane">Swaziland</option>
                                  <option <?php selected( $country, 'Sweden' ) ?> value="Sweden" data-alternative-spellings="SE Sverige" data-relevancy-booster="1.5">Sweden</option>
                                  <option <?php selected( $country, 'Switzerland' ) ?> value="Switzerland" data-alternative-spellings="CH Swiss Confederation Schweiz Suisse Svizzera Svizra" data-relevancy-booster="1.5">Switzerland</option>
                                  <option <?php selected( $country, 'Syrian Arab Republic' ) ?> value="Syrian Arab Republic" data-alternative-spellings="SY Syria سورية">Syrian Arab Republic</option>
                                  <option <?php selected( $country, 'Taiwan' ) ?> value="Taiwan" data-alternative-spellings="TW 台灣 臺灣">Taiwan</option>
                                  <option <?php selected( $country, 'Tajikistan' ) ?> value="Tajikistan" data-alternative-spellings="TJ Тоҷикистон Toçikiston">Tajikistan</option>
                                  <option <?php selected( $country, 'Tanzania, United Republic of' ) ?> value="Tanzania, United Republic of" data-alternative-spellings="TZ">Tanzania, United Republic of</option>
                                  <option <?php selected( $country, 'Thailand' ) ?> value="Thailand" data-alternative-spellings="TH ประเทศไทย Prathet Thai">Thailand</option>
                                  <option <?php selected( $country, 'Timor-Leste' ) ?> value="Timor-Leste" data-alternative-spellings="TL">Timor-Leste</option>
                                  <option <?php selected( $country, 'Togo' ) ?> value="Togo" data-alternative-spellings="TG Togolese">Togo</option>
                                  <option <?php selected( $country, 'Tokelau' ) ?> value="Tokelau" data-alternative-spellings="TK" data-relevancy-booster="0.5">Tokelau</option>
                                  <option <?php selected( $country, 'Tonga' ) ?> value="Tonga" data-alternative-spellings="TO">Tonga</option>
                                  <option <?php selected( $country, 'Trinidad and Tobago' ) ?> value="Trinidad and Tobago" data-alternative-spellings="TT">Trinidad and Tobago</option>
                                  <option <?php selected( $country, 'Tunisia' ) ?> value="Tunisia" data-alternative-spellings="TN تونس">Tunisia</option>
                                  <option <?php selected( $country, 'Turkey' ) ?> value="Turkey" data-alternative-spellings="TR Türkiye Turkiye">Turkey</option>
                                  <option <?php selected( $country, 'Turkmenistan' ) ?> value="Turkmenistan" data-alternative-spellings="TM Türkmenistan">Turkmenistan</option>
                                  <option <?php selected( $country, 'Turks and Caicos Islands' ) ?> value="Turks and Caicos Islands" data-alternative-spellings="TC" data-relevancy-booster="0.5">Turks and Caicos Islands</option>
                                  <option <?php selected( $country, 'Tuvalu' ) ?> value="Tuvalu" data-alternative-spellings="TV" data-relevancy-booster="0.5">Tuvalu</option>
                                  <option <?php selected( $country, 'Uganda' ) ?> value="Uganda" data-alternative-spellings="UG">Uganda</option>
                                  <option <?php selected( $country, 'Ukraine' ) ?> value="Ukraine" data-alternative-spellings="UA Ukrayina Україна">Ukraine</option>
                                  <option <?php selected( $country, 'United Arab Emirates' ) ?> value="United Arab Emirates" data-alternative-spellings="AE UAE الإمارات">United Arab Emirates</option>
                                  <option <?php selected( $country, 'United Kingdom' ) ?> value="United Kingdom" data-alternative-spellings="GB Great Britain England UK Wales Scotland Northern Ireland" data-relevancy-booster="2.5">United Kingdom</option>
                                  <option <?php selected( $country, 'United States' ) ?> value="United States" data-relevancy-booster="3.5" data-alternative-spellings="US USA United States of America">United States</option>
                                  <option <?php selected( $country, 'United States Minor Outlying Islands' ) ?> value="United States Minor Outlying Islands" data-alternative-spellings="UM">United States Minor Outlying Islands</option>
                                  <option <?php selected( $country, 'Uruguay' ) ?> value="Uruguay" data-alternative-spellings="UY">Uruguay</option>
                                  <option <?php selected( $country, 'Uzbekistan' ) ?> value="Uzbekistan" data-alternative-spellings="UZ Ўзбекистон O'zbekstan O‘zbekiston">Uzbekistan</option>
                                  <option <?php selected( $country, 'Vanuatu' ) ?> value="Vanuatu" data-alternative-spellings="VU">Vanuatu</option>
                                  <option <?php selected( $country, 'Venezuela' ) ?> value="Venezuela" data-alternative-spellings="VE">Venezuela</option>
                                  <option <?php selected( $country, 'Vietnam' ) ?> value="Vietnam" data-alternative-spellings="VN Việt Nam" data-relevancy-booster="1.5">Vietnam</option>
                                  <option <?php selected( $country, 'Virgin Islands, British' ) ?> value="Virgin Islands, British" data-alternative-spellings="VG" data-relevancy-booster="0.5">Virgin Islands, British</option>
                                  <option <?php selected( $country, 'Virgin Islands, U.S.' ) ?> value="Virgin Islands, U.S." data-alternative-spellings="VI" data-relevancy-booster="0.5">Virgin Islands, U.S.</option>
                                  <option <?php selected( $country, 'Wallis and Futuna' ) ?> value="Wallis and Futuna" data-alternative-spellings="WF" data-relevancy-booster="0.5">Wallis and Futuna</option>
                                  <option <?php selected( $country, 'Western Sahara' ) ?> value="Western Sahara" data-alternative-spellings="EH لصحراء الغربية">Western Sahara</option>
                                  <option <?php selected( $country, 'Yemen' ) ?> value="Yemen" data-alternative-spellings="YE اليمن">Yemen</option>
                                  <option <?php selected( $country, 'Zambia' ) ?> value="Zambia" data-alternative-spellings="ZM">Zambia</option>
                                  <option <?php selected( $country, 'Zimbabwe' ) ?> value="Zimbabwe" data-alternative-spellings="ZW">Zimbabwe</option>  
                               </select>
                              <p class="description"><?php _e( 'Select Country', 'rcp' ); ?></p>
                   </p>
                 </div>
               </td>

        </tr>
        <?php
}
add_action( 'rcp_edit_member_after', 'pw_rcp_add_member_edit_fields' );


//save data
/** 3
 * Stores the information submitted during registration
 *
 */
function pw_rcp_save_user_fields_on_register( $posted, $user_id ) {

        if( ! empty( $posted['rcp_dob'] ) ) {
                update_user_meta( $user_id, 'rcp_dob', sanitize_text_field( $posted['rcp_dob'] ) );
        }
        if( ! empty( $posted['rcp_gender'] ) ) {
                update_user_meta( $user_id, 'rcp_gender', sanitize_text_field( $posted['rcp_gender'] ) );
        }
         if( ! empty( $posted['rcp_website'] ) ) {
                update_user_meta( $user_id, 'rcp_website', sanitize_text_field( $posted['rcp_website'] ) );
        }
        if( ! empty( $posted['rcp_companyname'] ) ) {
                update_user_meta( $user_id, 'rcp_companyname', sanitize_text_field( $posted['rcp_companyname'] ) );
        }
        if( ! empty( $posted['rcp_telephone'] ) ) {
                update_user_meta( $user_id, 'rcp_telephone', sanitize_text_field( $posted['rcp_telephone'] ) );
        }
        if( ! empty( $posted['rcp_address1'] ) ) {
                update_user_meta( $user_id, 'rcp_address1', sanitize_text_field( $posted['rcp_address1'] ) );
        }
        if( ! empty( $posted['rcp_address2'] ) ) {
                update_user_meta( $user_id, 'rcp_address2', sanitize_text_field( $posted['rcp_address2'] ) );
        }
        if( ! empty( $posted['rcp_postcode'] ) ) {
                update_user_meta( $user_id, 'rcp_postcode', sanitize_text_field( $posted['rcp_postcode'] ) );
        }
        if( ! empty( $posted['rcp_city'] ) ) {
                update_user_meta( $user_id, 'rcp_city', sanitize_text_field( $posted['rcp_city'] ) );
        }
        if( ! empty( $posted['rcp_state'] ) ) {
                update_user_meta( $user_id, 'rcp_state', sanitize_text_field( $posted['rcp_state'] ) );
        }
        if( ! empty( $posted['rcp_country'] ) ) {
                update_user_meta( $user_id, 'rcp_country', sanitize_text_field( $posted['rcp_country'] ) );
        }

}
add_action( 'rcp_form_processing', 'pw_rcp_save_user_fields_on_register', 10, 2 );




//member or profile edit save
/**
 * Stores the information submitted profile update
 *
 */
function pw_rcp_save_user_fields_on_profile_save( $user_id ) {

        
        if( ! empty( $_POST['rcp_dob'] ) ) {
                update_user_meta( $user_id, 'rcp_dob', sanitize_text_field( $_POST['rcp_dob'] ) );
        }
        if( ! empty( $_POST['rcp_gender'] ) ) {
                update_user_meta( $user_id, 'rcp_gender', sanitize_text_field( $_POST['rcp_gender'] ) );
        }
        if( ! empty( $_POST['rcp_website'] ) ) {
                update_user_meta( $user_id, 'rcp_website', sanitize_text_field( $_POST['rcp_website'] ) );
        }
        if( ! empty( $_POST['rcp_companyname'] ) ) {
                update_user_meta( $user_id, 'rcp_companyname', sanitize_text_field( $_POST['rcp_companyname'] ) );
        }
        if( ! empty( $_POST['rcp_telephone'] ) ) {
                update_user_meta( $user_id, 'rcp_telephone', sanitize_text_field( $_POST['rcp_telephone'] ) );
        }
        if( ! empty( $_POST['rcp_address1'] ) ) {
                update_user_meta( $user_id, 'rcp_address1', sanitize_text_field( $_POST['rcp_address1'] ) );
        }
        if( ! empty( $_POST['rcp_address2'] ) ) {
                update_user_meta( $user_id, 'rcp_address2', sanitize_text_field( $_POST['rcp_address2'] ) );
        }
        if( ! empty( $_POST['rcp_postcode'] ) ) {
                update_user_meta( $user_id, 'rcp_postcode', sanitize_text_field( $_POST['rcp_postcode'] ) );
        }
        if( ! empty( $_POST['rcp_city'] ) ) {
                update_user_meta( $user_id, 'rcp_city', sanitize_text_field( $_POST['rcp_city'] ) );
        }
        if( ! empty( $_POST['rcp_state'] ) ) {
                update_user_meta( $user_id, 'rcp_state', sanitize_text_field( $_POST['rcp_state'] ) );
        }
        if( ! empty( $_POST['rcp_country'] ) ) {
                update_user_meta( $user_id, 'rcp_country', sanitize_text_field( $_POST['rcp_country'] ) );
        }

}
add_action( 'rcp_user_profile_updated', 'pw_rcp_save_user_fields_on_profile_save', 10 );
add_action( 'rcp_edit_member', 'pw_rcp_save_user_fields_on_profile_save', 10 );
